#!/bin/bash

# Script de Pós-Instalação Ubuntu 22.04
# por: Valter Maia da Silva

dir=$(dirname "$0")
DIR_THEME=$HOME/post-install

. $dir/functions/messages
. $dir/functions/dependencies
. $dir/functions/zsh_installer
. $dir/functions/asdf_installer
. $dir/functions/theme_installer
. $dir/functions/boxes_installer
. $dir/functions/plank_installer
. $dir/functions/ulauncher_installer
. $dir/functions/conky_installer

clear

if [ ! -e ~/post-install/.post-install-update.log ]; then
   echo_message warning "😏 Iniciando a atualização do sistema..."
   sleep 2
   sudo apt update -y
   sudo apt upgrade -y 
   sudo apt install dialog -y > ~/post-install/.post-install-update.log
   unzip $DIR_THEME/catppuccin-theme.zip -d $DIR_THEME
   cd $DIR_THEME
   echo ""
	read -p "📣 Tecle [Enter] para continuar" ready
fi

proxima=primeira

while : ; do
   case "$proxima" in 
      primeira )
         proxima=menu
         dialog --exit-label "Continuar" --textbox .asciiart 0 0
         ;;
      menu )
         anterior=primeira
         proxima=sair
         cmd=(dialog --title 'Menu de opções' --separate-output --checklist "\nMarque as ações que deseja executar:" 0 0 0)
         options=(1 "Atualizar repositórios e pacotes do sistema" off
            2 "Instalar dependências" off
            3 "Instalar o ZSH e Oh-My-ZSH" off
            4 "Instalar o ASDF (0.14.0)" off
            5 "Instalar o tema CATPPUCCIN" off
            6 "Instalar o CONKY" off
            7 "Instalar o PLANK (Dock)" off
            8 "Instalar o ULAUNCHER" off
            9 "Instalar o GNOME-BOXES (virtualização)" off)
         choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		   clear
		   for choice in $choices
		   do
		      case $choice in
               1 ) 
                  clear
	               echo ""
	               echo_message warning "😏 Iniciando a atualização do sistema..."
	               sleep 2
	               sudo apt update -y && sudo apt upgrade -y && sudo apt autoremove -y
	               echo ""
	               read -p "📣 Tecle [Enter] para continuar" ready
                  ;;
               2 ) 
                  dependencies
                  ;;
               3 ) 
                  zsh_install
                  ;;
               4 ) 
                  asdf_install
                  ;;
               5 ) 
                  theme_install
                  ;;
               6 ) 
                  conky_install
                  ;;
               7 ) 
                  plank_install
                  ;;
               8 ) 
                  ulauncher_install
                  ;;
               9 ) 
                  boxes
                  ;;
            esac
         done
         retorno=$?
         [ $retorno -eq 1   ] && proxima=$anterior
         [ $retorno -eq 255 ] && break
         ;;
   esac

   retorno=$?
   [ $retorno -eq 1   ] && proxima=$anterior
   [ $retorno -eq 255 ] && break
done

clear
neofetch

# UBUNTU POST-INSTALL SCRIPT

## Instalação


Abrir o console e colar o conteúdo:

```bash
cd ~ && \
wget https://gitlab.com/valtermaia/post-install/-/archive/main/post-install-main.zip && \
unzip post-install-main.zip && mv post-install-main post-install && rm ~/post-install-main.zip && \
cd ~/post-install && chmod +x functions/* && \
chmod +x start.sh && ./start.sh
```
